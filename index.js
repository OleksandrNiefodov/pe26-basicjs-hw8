/*Теоретичні питання
1. Опишіть своїми словами як працює метод forEach.

- цей метод очікує функцію (callback function) і виконує її з кожним елементом в масиві один раз. Не створює окремого масиву.

2. Які методи для роботи з масивом мутують існуючий масив, а які повертають новий масив? Наведіть приклади.
мутують
splice
push
pop
shift
unshift
reverse

не мутують
slice 
filter
map
reduce

3. Як можна перевірити, що та чи інша змінна є масивом?

- шкода, але typeof в цьому випадку не спрацює, тому що він видасть результат - об'єкт.
Тому можна використати Array.isArray:

const array = ['1', '2', '3'];
const isArray = Array.isArray(array);
console.log(isArray); // true

4. В яких випадках краще використовувати метод map(), а в яких forEach()?

- метод map краще викоистовувати коли треба отримати якийсь новий масив, з результатами виконання функції
-метод forEach навпаки коли не треба отримувати новий масив, коли є потреба щось зробити з кожним елементом масиву, але результат не треба зберігати 
*/


/*
Практичні завдання
1. Створіть масив з рядків "travel", "hello", "eat", "ski", "lift" та обчисліть кількість рядків з довжиною більше за 3 символи. Вивести це число в консоль.
*/

const randomArr = ['travel', 'hello', 'eat', 'ski', 'lift'];

const filteredByLength = randomArr.filter((elem) => elem.length > 3);
console.log(filteredByLength.length);

/*
2. Створіть масив із 4 об'єктів, які містять інформацію про людей: {name: "Іван", age: 25, sex: "чоловіча"}. Наповніть різними даними. Відфільтруйте його, щоб отримати 
тільки об'єкти зі sex "чоловіча".
Відфільтрований масив виведіть в консоль.
*/

const users = [
    {
       name: 'Oleksandr',
       age: 38,
       sex: 'male',   
    },
    {
       name: 'Maria',
       age: 38,
       sex: 'female',    
    },
    {
       name: 'Oleksiy',
       age: 40,
       sex: 'male',   
    },
    {
       name: 'Vadim',
       age: 30,
       sex: 'male',  
    },
    {
        name:'oksana',
        age: 45,
        sex: 'female',
    },
];

const filteredBySex = users.filter((elem) => elem.sex === 'male');
console.log(filteredBySex);

/*
3. Реалізувати функцію фільтру масиву за вказаним типом даних. (Опціональне завдання)
Технічні вимоги:
- Написати функцію filterBy(), яка прийматиме 2 аргументи. Перший аргумент - масив, який міститиме будь-які дані, другий аргумент - тип даних.
- Функція повинна повернути новий масив, який міститиме всі дані, які були передані в аргумент, за винятком тих, тип яких був переданий другим аргументом. Тобто якщо передати масив 
['hello', 'world', 23, '23', null], і другим аргументом передати 'string', то функція поверне масив [23, null].
*/


const filterBy = function(array, dataType) {
   return array.filter((elem) => typeof elem !== dataType);
};
console.log(filterBy(['some', 23, ,35, 'Dog', 'Rock' , null], `string`));

const array = ['1', '2', '3'];
const isArray = Array.isArray(array);
console.log(isArray);

